DATA_TYPES = {
    'string': str,
    'dictionary': dict,
    'integer': int,
    'float': float,
}
