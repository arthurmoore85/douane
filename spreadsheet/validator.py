"""
..module: .validator.py
..description: Validator library for Douane.
            This library allows the validation of data in a spreadsheet.

..author: Arthur Moore <arthur.moore85@gmail.com>
..date: 2021-06-20
"""
import xlrd
import yaml
from .converter import load_data
from .types import DATA_TYPES


class Validator(object):
    def __init__(self, *args, **kwargs):
        self.file_loc = kwargs.get('file_location')
        self._wb = xlrd.open_workbook(self.file_loc)
        self.data = load_data(self.file_loc) if self.file_loc else None
        self.validate_yaml = kwargs.get('validate_yaml')

    def _missing_file(self):
        """
        Raises error if file is missing.
        """
        raise IOError('Missing file_location!')

    def _yaml_extract(self):
        """
        Extracts data from yaml file.
        """
        with open(self.validate_yaml) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            
            return data
    
    def validate(self):
        """
        Validates the integrity of spreadsheet data
        according to the validation yaml.
        """
        validation_expects = self._yaml_extract()
        rules = {}
        sheet_index = 0
        for sheet in self.data:
            sheet_rules = {}
            headers = sheet.headers
            for head in headers:
                data_type_req = validation_expects.get(head)
                if data_type_req:
                    sheet_rules['index'] = headers.index(head)
                    sheet_rules['data_type'] = DATA_TYPES.get(data_type_req)
            rules[sheet_index] = sheet_rules
            sheet_index += 1
        
        print(rules)
