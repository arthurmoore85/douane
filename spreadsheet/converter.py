"""
..module: .converter.py
..description: Loads in a spreadsheet and converts data into objects.

..author: Arthur Moore <arthur.moore85@gmail.com>
..date: 2021-06-20
"""
import os
import xlrd

def load_data(file_location):
    """
    Loads the spreadsheet and converts them to
    Python objects.

    Args:
        file_location (string): Full path to the spreadsheet.

    Returns:
        data (list): List of Python objects representing the spreadsheet.
    """
    if not os.path.exists(file_location):
        raise IOError('File path can not be found.')
    
    wb = xlrd.open_workbook(file_location)
    spreadsheet_obj = DouaneSpreadsheet()
    sheets = []
    for sheet in wb.sheets():
        row_count = sheet.nrows
        col_count = sheet.ncols
        sheet_obj = DouaneSheet()
        sheet_obj.name = sheet.name
        row_sheet = []
        for cur_row in range(0, row_count):
            if cur_row == 0:
                header = sheet.row_values(0, start_colx=0, end_colx=None)
                sheet_obj.headers = header
                continue
            row_obj = DouaneRow()
            row_obj.row_no = cur_row
            cell_sheet = []
            for cell in range(0, col_count):
                cell_obj = sheet.cell(cur_row, cell)
                cell_value = cell_obj.value
                cell_data = DouaneData(value=cell_value)
                cell_sheet.append(cell_data)
            row_obj.cells = cell_sheet
            row_sheet.append(row_obj)
        sheet_obj.rows = row_sheet
        sheets.append(sheet_obj)
    spreadsheet_obj.sheets = sheets
    
    return spreadsheet_obj

class DouaneSpreadsheet(object):
    def __init__(self, *args, **kwargs):
        self.sheets = None

class DouaneSheet(object):
    """
    Sheet object that represents a sheet of spreadsheet data.
    """
    def __init__(self, *args, **kwargs):
        self.rows = None
        self.name = None
        self.headers = None

    def __repr__(self, *args, **kwargs):
        if self.name:
            return f"<Sheet: {self.name}>"
        else:
            return "Sheet - no name"


class DouaneRow(object):
    """
    Row object that represents a row of spreadsheet data
    """
    def __init__(self, *args, **kwargs):
        self.cells = None
        self.row_no = None

    def __repr__(self, *args, **kwargs):
        return f"<Row {self.row_no}>"


class DouaneData(object):
    """
    Data object that represents the data contained in a spreadsheet cell.
    """
    def __init__(self, *args, **kwargs):
        self.value = kwargs.get('value')

    def __str__(self, *args, **kwargs):
        return self.value

    def __repr__(self, **args):
        return f"{self.value}"
